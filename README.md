# Szkolenie Docker Wrzesien + Gulp + Bower

## Gulp + Bower + LESS      
## Konrad Bolek

```
# copy the files
git clone https://github.com/visiblevc/wordpress-starter.git

# navigate to example directory
cd wordpress-starter/example

# start the website at localhost:8080
docker-compose up -d && docker-compose logs -f wordpress
```

Wszystkie pliki znajdują się w katalogu _data 

> Zainstalować gulp'a

> Przeglądnąć taski gulp'a

> Skompilować body.scss z themem underscore (katalog /underscore/sass/)

> Skompilować jquery.js z themem underscore (katalog /underscore/js/)

> Zmniejsz logo polcode (katalog /underscore/images/)

